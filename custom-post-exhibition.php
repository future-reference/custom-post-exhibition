<?php

/**
 * @wordpress-plugin
 * Plugin Name:       Custom Post Type - Exhibition
 * Plugin URI:        https://gitlab.com/future-reference/custom-post-exhibition
 * Description:       Wordpress Plugin to create a custom post type named exhibition
 * Version:           1.0.4
 * Author:            Future Reference
 * Author URI:        http://futurereference.co/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       custom-post-exhibition
 */

add_action('init', 'exhibition_register_post_types');

function exhibition_register_post_types()
{

    /* Register the Exhibition post type. */

    register_post_type(
        'exhibition',
        array(
            'description'         => '',
            'public'              => true,
            'publicly_queryable'  => true,
            'show_in_nav_menus'   => false,
            'show_in_admin_bar'   => true,
            'exclude_from_search' => false,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_position'       => 20,
            'menu_icon'           => 'dashicons-screenoptions',
            'can_export'          => true,
            'delete_with_user'    => false,
            'hierarchical'        => false,
            'has_archive'         => true,
            'capability_type'     => 'post',

            /* The rewrite handles the URL structure. */
            'rewrite' => array(
                'slug'       => 'exhibition',
                'with_front' => false,
                'pages'      => true,
                'feeds'      => true,
                'ep_mask'    => EP_PERMALINK,
            ),

            /* What features the post type supports. */
            'supports' => array(
                'title',
                'editor',
                'revisions',
                'thumbnail'
            ),

            /* Labels used when displaying the posts. */
            'labels' => array(
                'name'               => __('Exhibitions',                    'futurereference'),
                'singular_name'      => __('Exhibition',                     'futurereference'),
                'menu_name'          => __('Exhibitions',                    'futurereference'),
                'name_admin_bar'     => __('Exhibitions',                    'futurereference'),
                'add_new'            => __('Add New',                        'futurereference'),
                'add_new_item'       => __('Add New Exhibition',             'futurereference'),
                'edit_item'          => __('Edit Exhibition',                'futurereference'),
                'new_item'           => __('New Exhibition',                 'futurereference'),
                'view_item'          => __('View Exhibition',                'futurereference'),
                'search_items'       => __('Search Exhibitions',             'futurereference'),
                'not_found'          => __('No exhibitions found',           'futurereference'),
                'not_found_in_trash' => __('No exhibitions found in trash',  'futurereference'),
                'all_items'          => __('Exhibitions',                    'futurereference'),
            )
        )
    );
}

add_action('init', 'exhibition_register_taxonomies');

function exhibition_register_taxonomies()
{

    /* Register the Exhibition Type taxonomy. */
    register_taxonomy(
        'exhibition_type',
        array( 'exhibition' ),
        array(
            'public'            => true,
            'show_ui'           => true,
            'show_in_menu'      => true,
            'show_in_nav_menus' => true,
            'show_tagcloud'     => false,
            'show_admin_column' => true,
            'hierarchical'      => true,
            /* The rewrite handles the URL structure. */
            'rewrite' => array(
                'slug'         => 'exhibitions',
                'with_front'   => false,
                'hierarchical' => true,
                'ep_mask'      => EP_NONE
            ),
            /* Labels used when displaying taxonomy and terms. */
            'labels' => array(
                'name'                       => __('Exhibition Types',          'futurereference'),
                'singular_name'              => __('Exhibition Type',           'futurereference'),
                'menu_name'                  => __('Exhibition Types',          'futurereference'),
                'name_admin_bar'             => __('Exhibition Type',           'futurereference'),
                'search_items'               => __('Search Exhibition Types',   'futurereference'),
                'popular_items'              => __('Popular Exhibition Types',  'futurereference'),
                'all_items'                  => __('All Exhibition Types',      'futurereference'),
                'edit_item'                  => __('Edit Exhibition Type',      'futurereference'),
                'view_item'                  => __('View Exhibition Type',      'futurereference'),
                'update_item'                => __('Update Exhibition Type',    'futurereference'),
                'add_new_item'               => __('Add New Exhibition Type',   'futurereference'),
                'new_item_name'              => __('New Exhibition Type',       'futurereference'),
                'parent_item_colon'          => __('',                          'futurereference'),
                'separate_items_with_commas' => null,
                'add_or_remove_items'        => null,
                'choose_from_most_used'      => null,
                'not_found'                  => null,
            )
        )
    );
}
